from config import config, independent_steps
from config_processor import get_configs
from data_processor import get_text
import preprocessor as p


preprocessed_dir = "data/preprocessed/"


def run():
    """
    Runs all configs
    :return:
    """
    configs = list(get_configs(config, independent_steps))[0:1]
    for cfg in configs:
        run_config(cfg)


def config_string(config):
    from urllib.parse import quote_plus
    text = ""
    for step in config:
        for option in config[step]:
            text += quote_plus(str(config[step][option]))
    return text


def save_data(filename, data):
    from pickle import dump
    from os.path import isfile

    if isfile(filename):
        return

    with open(filename, "wb") as file:
        dump(data, file)


def get_data(filename):
    from pickle import load

    with open(filename, "rb") as file:
        return load(file)


def run_config(configuration):
    from os.path import isfile
    from itertools import chain
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt

    preprocessed_filename = preprocessed_dir + config_string({"data":configuration["data"], "preprocess":configuration["preprocess"]})
    # print(preprocessed_filename)

    data = None
    tagged_words = None

    if isfile(preprocessed_filename + "_tags"):
        tagged_words = get_data(preprocessed_filename + "_tags")
    else:

        text = get_text(configuration["data"]["text"])
        print(text)

        cleaned = p.clean(text, configuration["preprocess"]["clean"])
        sentences = p.get_sentences(cleaned)
        words = p.get_words(sentences)
        tagged_words = p.get_pos_tags(words)
        save_data(preprocessed_filename + "_tags", tagged_words)
        lemmatized_words = p.lemmatize(tagged_words)

        if configuration["preprocess"]["ngrams"]:
            data = p.get_ngrams(lemmatized_words, configuration["preprocess"]["ngrams"])
            save_data(preprocessed_filename + "_ngrams", data)
        else:
            data = p.flatten_sentences(lemmatized_words)
            save_data(preprocessed_filename + "_lemmatized", data)


    tags = list(chain(*tagged_words))
    tags = pd.DataFrame(tags, columns=["word", "tag"], dtype="category")

    # remove end punctuation
    tags = tags[tags.tag != "."]
    tags.tag = tags.tag.cat.remove_unused_categories()

    tag_description = tags.describe()
    print(tag_description)

    # extract POS-tag counts
    tag_counts = tags.tag.value_counts(normalize=True)
    tag_counts.plot(title="POS-tag frequencies", kind="barh")
    plt.show()
    # remove bottom and top 5%
    tag_counts = tag_counts.loc[(tag_counts > np.percentile(tag_counts, 5)) & (tag_counts < np.percentile(tag_counts, 95))]
    tag_counts.plot(title="POS-tag frequencies without bottom and top 5 percentiles", kind="barh")
    plt.show()

    # word_counts = tags.word.value_counts(normalize=True)
    # word_counts.plot( title="Word frequencies", kind="barh")
    # plt.show()
    #print(word_counts)





if __name__ == '__main__':
    run()