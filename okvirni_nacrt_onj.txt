KONFIGURATOR:

config.file
preprocesing = {
	tokenizing = ["povedi", "n-gram"],
	stopwords = [True, False]
..
}

embedding = {
	doc2vec = {
		size: [100, 200, 300]
	}
}

#vrne vse kombinacije konfigov
# vrne seznam s slovarji kombinacij konfigov
combine_configs()

---------

ORKESTRATOR:
run_config(config)


--------

PREPROCESSING:
tokenizacija - po povedih (treniran tokenizer?)
	     - n-grami
odstranitev stop-wordov
čiščenje (odstranjevanje posebnih karakterjev)
	- argumenti (karakterji)

load_document()

save_document()

-------

EMBEDDING/VEKTORIZACIJA:
opt. 1: tf-idf
opt- 2: doc2vec (neural net - gensim)

-----


CLUSTERING:
# metoda ki vrne število clustrov z metodo
get_clusters(st_clustrov)

# izriši diagram
plot_clusters(clusters)

# prikaz klasifikacije na grafu
http://blog.aylien.com/wp-content/uploads/2016/06/Screen-Shot-2016-08-10-at-16.21.08.png
točka  - številka  (dejanski razred)
barva  - cluster   (napovedan razred)


------

KLASIFIKACIJA

# sklearn
# ločitev učne in testne množice
http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html

# random forest
# možne konfiguracije
	# n_estimators
	# max-features (domenvam da najboljsi sqrt(n_features))
	# max_depth (ali imava izbor pruninga / obrezovanja)

http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html

# poljubna druga metoda

------

RESULT PROCESSING:

podaš confusion-matrix
vrne vrednosti metrik npr. (accuracy, precision, recall, specificity, f1 score, matthews correlation coefficient)

get_metrics(confusion_matrix)

------








