text_dir = "data/texts/"

pride_and_prejudice = "pride_and_prejudice.txt"


def get_text(filename):
    text = ""
    with open(text_dir + filename, "r", encoding="utf-8-sig") as file:
        for line in file:
            text += line
    return text


if __name__ == '__main__':
    text = get_text(pride_and_prejudice)
    print(text)