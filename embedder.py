def embed_documents(documents):
    """
    Embeds documents into vectors
    :param documents:
    :return: document vector array
    """
    pass



def tokenizer(text):
    import preprocessor as p


def tf_idf():
    from sklearn.feature_extraction.text import TfidfVectorizer

    vectorizer = TfidfVectorizer(
        max_df=0.9,
        min_df=0.1,
        max_features=200000,
        stop_words="english",
        use_idf=True,
        ngram_range=(2, 4),
    )
