def clean(text, characters):
    """
    Lowercase words, removes provided characters
    :param text: input text
    :param characters: set of characters to remove
    :return: text
    """
    import re

    regex = re.compile('[%s]' % re.escape(characters))
    return regex.sub('', text).lower()


def get_sentences(text):
    """
    Splits text into sentences
    :param text: input text
    :return: sentence array
    """
    from nltk.tokenize import sent_tokenize

    return sent_tokenize(text)


def get_words(sentences):
    """
    Splits sentences into words
    :param sentences: sentence array
    :return: nested word array
    """
    from nltk.tokenize import word_tokenize

    return [word_tokenize(sentence) for sentence in sentences]


def get_ngrams(words, n):
    """
    Combines nested word array into n-grams
    :param words: nested word array
    :param n: number of windowed combinations
    :return: nested n-gram array
    """
    from nltk.util import ngrams

    return [list(ngrams(word_array, n)) for word_array in words]


def remove_stopwords(words):
    """
    Removes stopwords from text
    :param words: nested word array
    :return: text
    """
    from nltk.corpus import stopwords

    return [[word for word in word_array if word not in stopwords.words()] for word_array in words]


def get_pos_tags(words):
    """
    Tags words with part-of-speech
    Only makes sense to use when tokenized to sentences
    :param words: nested word array
    :return: tagged words in sentences
    """
    from nltk import pos_tag

    return [pos_tag(word_array) for word_array in words]


def lemmatize(tagged_words):
    """
    Lemmatizes words
    :param tagged_words: nested tagged word array
    :return: nested lemmatized word array
    """
    from nltk.stem import WordNetLemmatizer
    from nltk.corpus import wordnet

    lemmatizer = WordNetLemmatizer()

    def remap_pos_tag(tag):
        if tag.startswith("J"):
            return wordnet.ADJ
        elif tag.startswith("V"):
            return wordnet.VERB
        elif tag.startswith("N"):
            return wordnet.NOUN
        elif tag.startswith("R"):
            return wordnet.ADV
        else:
            return wordnet.NOUN

    return [[lemmatizer.lemmatize(word, pos=remap_pos_tag(tag))  for word, tag in word_array] for word_array in tagged_words]


def flatten_sentences(words):
    """
    Joins words inside nested arrays to sentence array
    :param words: nested word array
    :return: sentence array
    """

    return [" ".join(sentence) for sentence in words]


def save_preprocessed_data(filename, data):
    """
    Saved data to txt file
    :param filename: path to file
    :param data: data to be saved
    :return:
    """
    # filename should be a combination of selected preprocessor keys (config)

    pass


def run(text, config):
    cleaned = clean(text, config["clean"])
    sentences = get_sentences(cleaned)
    words = get_words(sentences)
    tagged_words = get_pos_tags(words)
    lemmatized_words = lemmatize(tagged_words)
    result = None
    if config["ngrams"]:
        result = get_ngrams(lemmatized_words, config["ngrams"])
    else:
        result = flatten_sentences(lemmatized_words)
    return result
    # TODO: save result


