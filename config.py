from string import punctuation
import re

# Define possible configurations for each step
config = {
    "data": {
        "text": ["pride_and_prejudice.txt"]
    },
    "preprocess": {
        "clean": [re.sub("[.!?\']", '', punctuation)],
        "ngrams": [None],
    },
    "embed": {
        "embed": [1]
    },
    "clustering": {
        "nclusters": [1]
    },
    "random_forest": {
        "n_estimators": [1]
    },
    "neural_network": {
        "learning_rate": [1]
    }
}

# Steps that shouldn't overlap
independent_steps = ["clustering", "random_forest", "neural_network"]