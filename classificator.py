def get_model(model_name):
    """
    Loads or creates a model
    :param model_name:
    :return: model
    """
    pass


def save_model(model, model_name):
    """
    Saves model
    :param model:
    :param model_name:
    :return:
    """
    pass


def split_train_test(data_x, data_y):
    """
    Splits data to train and test sets
    :param data_x:
    :param data_y:
    :return: train_x, train_y, test_x, test_y
    """
    pass


def train(model, train_x, train_y, params):
    """
    Trains a classification model on train set based on given params
    :param model:
    :param train_x:
    :param train_y:
    :param params:
    :return: trained model
    """
    pass


def test(model, test_x, test_y):
    """
    Runs predictions with given model on test set
    :param model:
    :param test_x:
    :param test_y:
    :return: confusion_matrix
    """
    pass
