from tests import texts
import preprocessor as p
from string import punctuation
import re


def test_clean():
    c1 = re.sub("[.\']", '', punctuation)
    print(p.clean(texts.t1, c1))
    print(p.clean(texts.t2, c1))


def test_get_sentences():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        print(p.get_sentences(cleaned))


def test_get_words():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        sentences = p.get_sentences(cleaned)
        print(p.get_words(sentences))


def test_get_ngrams():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        sentences = p.get_sentences(cleaned)
        words = p.get_words(sentences)
        print(p.get_ngrams(words, 3))

def test_flatten_sentences():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        sentences = p.get_sentences(cleaned)
        words = p.get_words(sentences)
        print(p.flatten_sentences(words))


def test_remove_stopwords():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        sentences = p.get_sentences(cleaned)
        words = p.get_words(sentences)
        print(p.remove_stopwords(words))


def test_get_pos_tags():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        sentences = p.get_sentences(cleaned)
        words = p.get_words(sentences)
        print(p.get_pos_tags(words))


def test_lemmatize():
    text_list = [texts.t1, texts.t2, texts.t3, texts.t4]
    c1 = re.sub("[.\']", '', punctuation)
    for text in text_list:
        cleaned = p.clean(text, c1)
        sentences = p.get_sentences(cleaned)
        words = p.get_words(sentences)
        tagged_words = p.get_pos_tags(words)
        print(p.lemmatize(tagged_words))


def test_run():
    config = {
        "clean": re.sub("[.\']", '', punctuation),
        "ngrams": None
    }
    print(p.run(texts.t4, config))
    # TODO: how to decode (and decode) these characters into a filename friendly string !"#$%&()*+,-/:;<=>?@[\\]^_`{|}~
    print(config)




if __name__ == '__main__':
    # test_clean()
    # test_get_sentences()
    # test_get_words()
    # test_get_ngrams()
    # test_flatten_sentences()
    # test_get_pos_tags()
    # test_remove_stopwords()
    # test_lemmatize()
    test_run()

