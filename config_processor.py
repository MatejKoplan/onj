def get_configs(config_setup, independent_steps):
    """
    Combines all possible configs from config setup
    :param config_setup: (max depth = 2)
    :param independent_steps: list of steps that shouldn't overlap
    :return: config array
    """
    from copy import deepcopy
    from itertools import chain

    # Save all general step names
    steps = []
    for step_key in config_setup:
        steps.append(step_key)

    # Generate individual step combinations
    step_combinations = {}
    for step in steps:
        step_combinations[step] = get_combinations(config_setup[step])

    # Exclude steps that shouldn't overlap
    # Remove them from previously generated combinations
    independent_combinations = {independent_step: step_combinations.pop(independent_step) for independent_step in independent_steps}

    # Combine each individual combination with base steps
    combinations = []
    for independent_combination in independent_combinations:
        # Copy base steps
        final_combination = deepcopy(step_combinations)
        # Append individual step to base step
        final_combination[independent_combination] = independent_combinations[independent_combination]
        # Produce final combinations
        combinations.append(get_combinations(final_combination))

    # Flatten list of lists
    return chain(*combinations)


def get_combinations(step_config):
    """
    produces all combinations of elements inside lists of a dict
    :param step_config: dict {key: [e1, e2], ...}
    :return:
    """
    from itertools import product

    keys = []
    options = []

    for key in step_config:
        keys.append(key)
        options.append(step_config[key])

    return [{key: value for key, value in zip(keys, combination)} for combination in product(*options)]
