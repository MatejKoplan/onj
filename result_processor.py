def get_metrics(confusiom_matrix):
    """
    Calculates
        -   Accuracy
        -   Precision
        -   Recall
        -   F1 score
        -   Matthews correlation coefficient
        -   Kappa statistic
    :param confusiom_matrix:
    :return: metrics DataFrame
    """
    pass


def get_best(metrics, metric_name):
    """
    Finds the result metric with highest value
    :param metrics:
    :param metric_name:
    :return: metric
    """