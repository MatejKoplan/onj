from config import config, independent_steps
from config_processor import get_configs
from data_processor import get_text
from preprocessor import run as preprocess


preprocessed_dir = "data/preprocessed/"


def run():
    """
    Runs all configs
    :return:
    """
    configs = list(get_configs(config, independent_steps))[0:1]
    for cfg in configs:
        run_config(cfg)


def config_string(config):
    from urllib.parse import quote_plus
    text = ""
    for step in config:
        for option in config[step]:
            text += quote_plus(str(config[step][option]))
    return text


def save_data(filename, data):
    from pickle import dump
    from os.path import isfile

    if isfile(filename):
        return

    with open(filename, "wb") as file:
        dump(data, file)


def get_data(filename):
    from pickle import load

    with open(filename, "rb") as file:
        return load(file)


def run_config(configuration):
    from os.path import isfile

    print(configuration)

    preprocessed_filename = preprocessed_dir + config_string({"data":configuration["data"], "preprocess":configuration["preprocess"]})
    print(preprocessed_filename)

    data = None

    if isfile(preprocessed_filename):
        data = get_data(preprocessed_filename)
    else:

        text = get_text(configuration["data"]["text"])
        print(text)

        data = preprocess(text, configuration["preprocess"])
        print(data)

        save_data(preprocessed_filename, data)
    print(data)


if __name__ == '__main__':
    run()